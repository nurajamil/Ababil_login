	<#import "template.ftl" as layout>
	<@layout.registrationLayout displayInfo=social.displayInfo; section>
		<#if section = "title">
			${msg("loginTitle",(realm.displayName!''))}
		<#elseif section = "header">
			${msg("loginTitleHtml",(realm.displayNameHtml!''))}
		<#elseif section = "form">
			<#if realm.password>
							<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
							<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
							<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
  
			
			<form id="kc-form-login" class="form-horizontal" action="${url.loginAction}" method="post" >
			
			
			
			<div id="kc-logo"><img src="${url.resourcesPath}/img/logo2x.png"/><div id="kc-logo-wrapper"></div></a></div>
			
				
					<div class="form-group">
						<div class='input-field col s12'>
							<label for="username" class="active"><#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if></label>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
							<#if usernameEditDisabled??>
								<input id="username" class="form-control" name="username" value="${(login.username!'')?html}" type="text" disabled />
							<#else>
								<input id="username" class="form-control" name="username" value="${(login.username!'')?html}" type="text" autofocus autocomplete="off" />
							</#if>
						</div>
					</div>

					<div class="form-group">
						<div class='input-field col m12'>
							<label for="password" class="active">${msg("password")}</label>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
							<input id="password" class="form-control" name="password" type="password" autocomplete="off" />
						</div>
					</div>

					<div class="form-group">
						<div id="kc-form-options" class="col-xs-4 col-sm-5 col-md-offset-4 col-md-4 col-lg-offset-3 col-lg-5">
							<#if realm.rememberMe && !usernameEditDisabled??>
								<div class="checkbox">
									<label>
										<#if login.rememberMe??>
											<input id="rememberMe" name="rememberMe" type="checkbox" tabindex="3" checked> ${msg("rememberMe")}
										<#else>
											<input id="rememberMe" name="rememberMe" type="checkbox" tabindex="3"> ${msg("rememberMe")}
										</#if>
									</label>
								</div>
							</#if>
							<div class="${properties.kcFormOptionsWrapperClass!}">
								<#if realm.resetPasswordAllowed>
									<span><a href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></span>
								</#if>
							</div>
						</div>

						<div id="kc-form-buttons"  class="col-xs-8 col-sm-7 col-md-4 col-lg-4 submit">
							<div class="${properties.kcFormButtonsWrapperClass!}">
								<input class="btn btn-primary btn-lg" name="login" id="kc-login" type="submit" value="${msg("doLogIn")}"/>
							</div>
						 </div>
					</div>
					
					<div>
					<#if realm.password && realm.registrationAllowed && !usernameEditDisabled??>
				<div id="kc-registration">
					<span>${msg("noAccount")} <a href="${url.registrationUrl}">${msg("doRegister")}</a></span>
				</div>
			</#if>
					</div>
					
					
					 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
					<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
				</form>
		   

			</#if>
		<#elseif section = "info" >
			

			<#if realm.password && social.providers??>
				<div id="kc-social-providers">
					<ul>
						<#list social.providers as p>
							<li><a href="${p.loginUrl}" id="zocial-${p.alias}" class="zocial ${p.providerId}"> <span class="text">${p.displayName}</span></a></li>
						</#list>
					</ul>
				</div>
			</#if>
		</#if>
	</@layout.registrationLayout>
